--
-- PostgreSQL database dump
--

-- Dumped from database version 10.6
-- Dumped by pg_dump version 10.6 (Ubuntu 10.6-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: awb_status_group_item; Type: TABLE DATA; Schema: public; Owner: sicepatstaging
--

INSERT INTO public.awb_status_group_item (awb_status_group_item_id, awb_status_group_id, awb_status_id, user_id_created, created_time, user_id_updated, updated_time, is_deleted) VALUES (1, 1, 1500, 1, '2018-09-19 12:00:00', 1, '2018-09-19 12:00:00', false);
INSERT INTO public.awb_status_group_item (awb_status_group_item_id, awb_status_group_id, awb_status_id, user_id_created, created_time, user_id_updated, updated_time, is_deleted) VALUES (2, 1, 3500, 1, '2018-09-19 12:00:00', 1, '2018-09-19 12:00:00', false);
INSERT INTO public.awb_status_group_item (awb_status_group_item_id, awb_status_group_id, awb_status_id, user_id_created, created_time, user_id_updated, updated_time, is_deleted) VALUES (3, 1, 2500, 1, '2018-09-19 12:00:00', 1, '2018-09-19 12:00:00', false);
INSERT INTO public.awb_status_group_item (awb_status_group_item_id, awb_status_group_id, awb_status_id, user_id_created, created_time, user_id_updated, updated_time, is_deleted) VALUES (4, 1, 13000, 1, '2018-09-19 12:00:00', 1, '2018-09-19 12:00:00', false);
INSERT INTO public.awb_status_group_item (awb_status_group_item_id, awb_status_group_id, awb_status_id, user_id_created, created_time, user_id_updated, updated_time, is_deleted) VALUES (5, 2, 3000, 1, '2018-09-19 12:00:00', 1, '2018-09-19 12:00:00', false);
INSERT INTO public.awb_status_group_item (awb_status_group_item_id, awb_status_group_id, awb_status_id, user_id_created, created_time, user_id_updated, updated_time, is_deleted) VALUES (6, 2, 3005, 1, '2018-09-19 12:00:00', 1, '2018-09-19 12:00:00', false);
INSERT INTO public.awb_status_group_item (awb_status_group_item_id, awb_status_group_id, awb_status_id, user_id_created, created_time, user_id_updated, updated_time, is_deleted) VALUES (7, 2, 3010, 1, '2018-09-19 12:00:00', 1, '2018-09-19 12:00:00', false);
INSERT INTO public.awb_status_group_item (awb_status_group_item_id, awb_status_group_id, awb_status_id, user_id_created, created_time, user_id_updated, updated_time, is_deleted) VALUES (8, 2, 14000, 1, '2018-09-19 12:00:00', 1, '2018-09-19 12:00:00', false);


--
-- PostgreSQL database dump complete
--

