export interface SwaggerDocumentOptions {
  include?: Function[];
  convertModelObjectKeysCase?: 'snake_case' | 'camelCase';
}
