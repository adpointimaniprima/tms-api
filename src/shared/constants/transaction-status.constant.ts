// table transaction status with filter status category COD_PAYMENT
export const TRANSACTION_STATUS = {
  SIGESIT: 30000,
  TRM: 31000,
  CANHO: 32500,
  TRF: 35000,
  TRMHO: 40000,
  CANCEL_DRAFT: 40500,
  DRAFT_INV: 41000,
  VOID: 41500,
  PAIDHO: 45000,
};
