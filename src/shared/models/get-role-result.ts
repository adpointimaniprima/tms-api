export class GetRoleResult {
  clientId: string;
  userId: number;
  email: string;
  username: string;
  displayName: string;
  roles: object[];
}
