export class GetAccessResult {
  clientId: string;
  userId: string;
  email: string;
  username: string;
  displayName: string;
  branchId: number;
  branchName: string;
  branchCode: string;
  permissionToken: string;
  rolesAccessPermissions: string[];
}
