export class AuthMetadata {
  clientId: string;
  userId: string;
  branchId: string;
  roles: string[];
  rolesAccessPermissions: string[];
}
