module.exports = {
  enabled: true,
  port: 4000,
  swagger: {
    enabled: true,
    title: "SiCepat TMS Auth",
    description: "SiCepat TMS auth server api",
    path: "docs",
  },
};
