module.exports = {
  enabled: true,
  port: 4002,
  swagger: {
    enabled: true,
    title: "SiCepat TMS Qeueu",
    description: "SiCepat TMS queue server api",
    path: "docs",
  },
};
