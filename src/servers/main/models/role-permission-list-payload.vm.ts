import { ApiModelProperty } from '../../../shared/external/nestjs-swagger';

export class RolePermissionListPayloadVm {
  @ApiModelProperty()
  roleId: number;
}
