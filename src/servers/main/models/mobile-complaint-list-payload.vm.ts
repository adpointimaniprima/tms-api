import { ApiModelProperty } from '../../../shared/external/nestjs-swagger';

export class MobileComplaintListPayloadVm {
  @ApiModelProperty()
  createdTime: string;

}
