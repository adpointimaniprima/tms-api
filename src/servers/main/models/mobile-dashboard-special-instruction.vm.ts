import { ApiModelProperty } from '../../../shared/external/nestjs-swagger';

export class MobileDashboardSpecialInstructionVm {
  @ApiModelProperty()
  SpecialInstruction: string;

}
