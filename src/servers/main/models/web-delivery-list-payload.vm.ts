import { ApiModelProperty } from '../../../shared/external/nestjs-swagger';

export class WebDeliveryList {
  @ApiModelProperty()
  doPodId: number;
}
