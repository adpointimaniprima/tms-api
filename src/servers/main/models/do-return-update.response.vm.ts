import { ApiModelProperty } from '../../../shared/external/nestjs-swagger';

export class ReturnUpdateFindAllResponseVm {
  @ApiModelProperty()
  message: string;

  @ApiModelProperty()
  status: string;

  @ApiModelProperty()
  doId: string;

}
