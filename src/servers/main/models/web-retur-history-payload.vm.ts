import { ApiModelProperty} from '../../../shared/external/nestjs-swagger';

export class WebReturHistoryPayloadVm {
  @ApiModelProperty()
  awbNumber: string;
}
