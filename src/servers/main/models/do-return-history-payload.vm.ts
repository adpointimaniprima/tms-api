import { ApiModelProperty, ApiModelPropertyOptional } from '../../../shared/external/nestjs-swagger';
export class ReturnHistoryPayloadVm {
  @ApiModelProperty()
  doReturnAwbId: string;
}
