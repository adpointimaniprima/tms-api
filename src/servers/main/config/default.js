module.exports = {
  enabled: true,
  port: 4001,
  swagger: {
    enabled: true,
    title: "SiCepat TMS Main",
    description: "SiCepat TMS main server api",
    path: "docs",
  },
  bullBoard: true,
};
