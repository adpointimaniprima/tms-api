module.exports = {
  enabled: true,
  port: 5000,
  swagger: {
    enabled: true,
    title: "SiCepat TMS Background",
    description: "SiCepat TMS backgound server",
    path: "docs",
  },
};
