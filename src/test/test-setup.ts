import { EntityFactory } from '@entity-factory/core';
import { TypeormAdapter } from '@entity-factory/typeorm';
import { HttpStatus } from '@nestjs/common';
import { forEach } from 'lodash';

import { boot } from '../main-setup';
import { AuthLoginResponseVM, PermissionAccessResponseVM } from '../servers/auth/models/auth.vm';
import { AwbBlueprint } from './blueprint/awb';
import { AwbAttrBlueprint } from './blueprint/awb-attr';
import { AwbItemBlueprint } from './blueprint/awb-item';
import { AwbItemAttrBlueprint } from './blueprint/awb-item-attr';
import { BagBlueprint } from './blueprint/bag';
import { BagItemBlueprint } from './blueprint/bag-item';
import { BranchBlueprint } from './blueprint/branch';
import { CustomerBlueprint } from './blueprint/customer';
import { EmployeeBlueprint } from './blueprint/employee';
import { PartnerLogisticBlueprint } from './blueprint/partner-logistic';
import { ReasonBlueprint } from './blueprint/reason';
import { RepresentativeBlueprint } from './blueprint/representative';
import { RoleBlueprint } from './blueprint/role';
import { UserBlueprint } from './blueprint/user';
import { UserRoleBlueprint } from './blueprint/user-role';
import TEST_GLOBAL_VARIABLE from './test-global-variable';
import { testInitDb } from './test-init-db';
import { TestUtility } from './test-utility';

process.env.NODE_ENV = 'test';

jest.setTimeout(5 * 60 * 1000);

beforeAll(async () => {
  if (process.env.RESET_DB) {
    await testInitDb();
  }

  const ormConfig = require('../../ormconfig.test');
  TEST_GLOBAL_VARIABLE.entityFactory = new EntityFactory({
    adapter: new TypeormAdapter(ormConfig),
    blueprints: [
      AwbAttrBlueprint,
      AwbBlueprint,
      AwbItemAttrBlueprint,
      AwbItemBlueprint,
      BagBlueprint,
      BagItemBlueprint,
      BranchBlueprint,
      CustomerBlueprint,
      EmployeeBlueprint,
      PartnerLogisticBlueprint,
      ReasonBlueprint,
      RepresentativeBlueprint,
      RoleBlueprint,
      UserBlueprint,
      UserRoleBlueprint,
    ],
  });

  const serverModules = await boot();
  TEST_GLOBAL_VARIABLE.serverModules = serverModules;

  await TestUtility.getUnauthenticatedAuthServerAxios()
    .post('/auth/login', {
      clientId: 'web',
      username: 'adry',
      password: 'qwerty',
    })
    .then(response => {
      expect(response.status).toEqual(HttpStatus.OK);

      const result = response.data as AuthLoginResponseVM;
      expect(result.userId).toEqual('15');
      expect(result.accessToken).toBeDefined();
      expect(result.refreshToken).toBeDefined();
      expect(result.username).toEqual('adry');

      TEST_GLOBAL_VARIABLE.webUserLogin = result;
    });

  await TestUtility.getUnauthenticatedAuthServerAxios()
    .post('/auth/login', {
      clientId: 'mobile',
      username: 'adry',
      password: 'qwerty',
    })
    .then(response => {
      expect(response.status).toEqual(HttpStatus.OK);

      const result = response.data as AuthLoginResponseVM;
      expect(result.userId).toEqual('15');
      expect(result.accessToken).toBeDefined();
      expect(result.refreshToken).toBeDefined();
      expect(result.username).toEqual('adry');

      TEST_GLOBAL_VARIABLE.mobileUserLogin = result;
    });

  await TestUtility.getAuthenticatedAuthServerAxios('web')
    .post('/auth/permissionAccess', {
      clientId: 'web',
      roleId: 11,
      branchId: 121,
    })
    .then(response => {
      expect(response.status).toEqual(HttpStatus.OK);

      const result = response.data as PermissionAccessResponseVM;
      expect(result.branchCode).toEqual('3601001');
      expect(result.branchName).toEqual('Kantor Pusat');
      expect(result.userId).toEqual('15');
      expect(result.clientId).toEqual('web');
      expect(result.username).toEqual('adry');
      expect(result.permissionToken).toBeDefined();
      expect(result.roleName).toEqual('Root IT');

      TEST_GLOBAL_VARIABLE.webUserPermissionToken = result.permissionToken;
    });

  await TestUtility.getAuthenticatedAuthServerAxios('mobile')
    .post('/auth/permissionAccess', {
      clientId: 'mobile',
      roleId: 11,
      branchId: 121,
    })
    .then(response => {
      expect(response.status).toEqual(HttpStatus.OK);

      const result = response.data as PermissionAccessResponseVM;
      expect(result.branchCode).toEqual('3601001');
      expect(result.branchName).toEqual('Kantor Pusat');
      expect(result.userId).toEqual('15');
      expect(result.clientId).toEqual('mobile');
      expect(result.username).toEqual('adry');
      expect(result.permissionToken).toBeDefined();
      expect(result.roleName).toEqual('Root IT');

      TEST_GLOBAL_VARIABLE.mobileUserPermissionToken = result.permissionToken;
    });
});

afterAll(async () => {
  forEach(TEST_GLOBAL_VARIABLE.serverModules, (serverModule: any) => {
    serverModule.stopServer();
  });
});
